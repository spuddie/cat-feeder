/*Cat Feeder
*/

#include <Servo.h>

// set constant variables for the input and output pins

const int switchPin = 2;
const int servoPin = 3;

// set variables to write data from switchPin(debounce)

int debounce1 = 0;
int debounce2 = 0;

//assign servo library to servo function

Servo servo;

void setup ()
{
  //Initialize servo and set pin for servo to pin 3
  servo.attach(servoPin);
  //Initialize pin 2 as output and assign to variable switchPin
  pinMode(switchPin,INPUT);
}

void loop()
{
/*Read switchPin twice with a delay in between readings,
this eliminates a false reading from the switch
*/  
 debounce1 = digitalRead(switchPin);
 delay(25);
 debounce2 = digitalRead(switchPin);
 // wait for a switch press if pressed and passes debounce test activate servo
 if (debounce1 == HIGH && debounce2 == HIGH){
   servo.write(40);
   delay(1500);
}else
servo.write(90);
}
  
